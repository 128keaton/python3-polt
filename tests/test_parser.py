# system modules
import unittest
import io
import re

# internal modules
from polt.parser.numberparser import NumberParser
from polt.parser.csvparser import CsvParser

# external modules


class ParserTest(unittest.TestCase):
    pass


class NumberParserTest(ParserTest):
    def test_parsing_positive(self):
        s = "1 2 3\n4 5.1234 6\n7.1\n8\n9\n\n\n10.4321"
        parser = NumberParser(io.StringIO(s))
        self.assertEqual(
            list(parser.data),
            [
                {(parser.quantity, parser.unit, parser.key): L}
                for L in [
                    [1, 2, 3],
                    [4, 5.1234, 6],
                    [7.1],
                    [8],
                    [9],
                    [10.4321],
                ]
            ],
        )

    def test_parsing_positive_and_negative(self):
        s = "-1 2 -3\n4 -5.1234 6\n-7.1\n-8\n-9\n\n\n10.4321"
        parser = NumberParser(io.StringIO(s))
        self.assertEqual(
            list(parser.data),
            [
                {(parser.quantity, parser.unit, parser.key): L}
                for L in [
                    [-1, 2, -3],
                    [4, -5.1234, 6],
                    [-7.1],
                    [-8],
                    [-9],
                    [10.4321],
                ]
            ],
        )


class CsvParserTest(ParserTest):
    def test_parse_header_default(self):
        parser = CsvParser()
        for header in ("quantity_unit", "sensor-quantity-unit", "quantity"):
            with self.subTest(header=header):
                self.assertTupleEqual(
                    parser.parse_header(header), (header, None, None)
                )

    def test_parse_header_unit_last(self):
        parser = CsvParser()
        for header, result in {
            "quantity_unit": ("quantity", "unit", None),
            "sensor_quantity_unit": ("sensor_quantity", "unit", None),
            "sensor-quantity-unit": ("sensor-quantity", "unit", None),
            "quantity": ("quantity", None, None),
            "quantity unit": ("quantity", "unit", None),
        }.items():
            with self.subTest(header=header):
                self.assertTupleEqual(
                    parser.parse_header(
                        header, regex=parser.HEADER_REGEX_UNIT_LAST
                    ),
                    result,
                )

    def test_parse_header_key_quantity_unit(self):
        parser = CsvParser()
        for header, result in {
            "sensor_quantity_unit": ("quantity", "unit", "sensor"),
            "sensor-quantity-unit": ("quantity", "unit", "sensor"),
            "sensor quantity unit": ("quantity", "unit", "sensor"),
            "key": (None, None, "key"),
            "key quantity": ("quantity", None, "key"),
        }.items():
            with self.subTest(header=header):
                self.assertTupleEqual(
                    parser.parse_header(
                        header, regex=parser.HEADER_REGEX_KEY_QUANTITY_UNIT
                    ),
                    result,
                )

    def test_parsing(self):
        s = "a,b,c\n1,2,3\n4,5,6\n7,8,9"
        parser = CsvParser(io.StringIO(s))
        self.assertEqual(
            list(map(dict, parser.data)),
            [
                {
                    ("a", None, None): 1,
                    ("b", None, None): 2,
                    ("c", None, None): 3,
                },
                {
                    ("a", None, None): 4,
                    ("b", None, None): 5,
                    ("c", None, None): 6,
                },
                {
                    ("a", None, None): 7,
                    ("b", None, None): 8,
                    ("c", None, None): 9,
                },
            ],
        )

    def test_only_columns(self):
        for only_columns in ("a,c", set(["a", "c"]), ["a", "c"]):
            with self.subTest(only_columns=only_columns):
                s = "a,b,c\n1,2,3\n4,5,6\n7,8,9"
                parser = CsvParser(io.StringIO(s), only_columns=only_columns)
                self.assertEqual(
                    list(map(dict, parser.data)),
                    [
                        {("a", None, None): 1, ("c", None, None): 3},
                        {("a", None, None): 4, ("c", None, None): 6},
                        {("a", None, None): 7, ("c", None, None): 9},
                    ],
                )

    def test_not_columns(self):
        for not_columns in ("a,c", set(["a", "c"]), ["a", "c"]):
            with self.subTest(not_columns=not_columns):
                s = "a,b,c\n1,2,3\n4,5,6\n7,8,9"
                parser = CsvParser(io.StringIO(s), not_columns=not_columns)
                self.assertEqual(
                    list(map(dict, parser.data)),
                    [
                        {("b", None, None): 2},
                        {("b", None, None): 5},
                        {("b", None, None): 8},
                    ],
                )

    def test_args(self):
        s = "a;b;c\n1;2;3\n4;5;6\n7;8;9"
        parser = CsvParser(io.StringIO(s), delimiter=";")
        self.assertEqual(
            list(map(dict, parser.data)),
            [
                {
                    ("a", None, None): 1,
                    ("b", None, None): 2,
                    ("c", None, None): 3,
                },
                {
                    ("a", None, None): 4,
                    ("b", None, None): 5,
                    ("c", None, None): 6,
                },
                {
                    ("a", None, None): 7,
                    ("b", None, None): 8,
                    ("c", None, None): 9,
                },
            ],
        )
