# system modules
import unittest
import threading
import io
import subprocess
import time
import logging

# internal modules
from polt.streamer import Streamer
from polt.parser.numberparser import NumberParser

# external modules


class StreamerTest(unittest.TestCase):
    def setUp(self):
        self.cat = subprocess.Popen(
            ["cat"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        self.parser = NumberParser(self.cat.stdout)
        self.streamer = Streamer(self.parser)

    def tearDown(self):
        self.cat.terminate()
        self.streamer.stop()

    def test_streamer(self):
        self.streamer.start()
        self.assertListEqual(list(self.streamer.buffer), [])
        self.cat.stdin.write("1 2 3\r\n\n".encode())
        self.cat.stdin.flush()
        time.sleep(0.1)
        buf = list(self.streamer.buffer)
        self.assertEqual(len(buf), 1)
        dataset = next(iter(buf))
        data = dataset.pop("data", None)
        self.assertEqual(
            data,
            {
                (self.parser.quantity, self.parser.unit, self.parser.key): [
                    1,
                    2,
                    3,
                ]
            },
        )
        self.streamer.buffer.pop()
        self.cat.stdin.write("1\n2\n3\n".encode())
        self.cat.stdin.flush()
        time.sleep(0.1)
        buf = list(self.streamer.buffer)
        self.assertEqual(len(buf), 3)
        for i, dataset in enumerate(buf):
            data = dataset.pop("data", None)
            self.assertEqual(
                data,
                {
                    (
                        self.parser.quantity,
                        self.parser.unit,
                        self.parser.key,
                    ): [i + 1]
                },
            )
