# system modules
import unittest

# internal modules
from polt.config import Configuration

# external modules


class ConfigTest(unittest.TestCase):
    def setUp(self):
        self.c = Configuration()
        self.c.read_dict(
            {
                "plot": {"interval": "100"},
                Configuration.SOURCE_SECTION_PREFIX
                + ":1": {"command": "cat /dev/urandom", "parser": "numbers"},
                Configuration.SOURCE_SECTION_PREFIX
                + ":2": {"command": "cat /dev/random", "parser": "numbers"},
            }
        )

    def test_source_section(self):
        self.assertEqual(len(list(self.c.source_section)), 2)

    def test_matching_source_section(self):
        self.assertEqual(
            len(
                list(
                    self.c.matching_source_section(
                        command="cat '/dev/urandom'"
                    )
                )
            ),
            1,
        )
        self.assertEqual(
            len(list(self.c.matching_source_section(parser="numbers"))), 2
        )
