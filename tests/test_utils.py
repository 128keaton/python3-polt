# system modules
import unittest

# internal modules
from polt.utils import flatten, normalize_cmd, str2num, to_float

# external modules


class UtilsTest(unittest.TestCase):
    def test_flatten(self):
        self.assertListEqual(
            flatten([0, [1, 2, 3], [4, [5, 6, [7, 8, 9]]]]), list(range(10))
        )

    def test_normalize_cmd(self):
        self.assertEqual(
            normalize_cmd("find 'folder'    -iname '*file.py' "),
            "find folder -iname '*file.py'",
        )

    def test_str2num(self):
        inputs = {int: "3", float: "3.456", str: "asdf"}
        for t, s in inputs.items():
            with self.subTest(input=t):
                converted = str2num(s)
                should_be = t(s)
                self.assertEqual(
                    converted,
                    should_be,
                    "str2num did not convert {} to {} but to {}".format(
                        repr(s), should_be, converted
                    ),
                )
                self.assertIs(
                    type(converted),
                    t,
                    "Instead of {}, "
                    "str2num converted {} to type {}".format(
                        t.__name__, repr(s), type(converted).__name__
                    ),
                )

    def test_to_float(self):
        self.assertEqual(to_float(1), 1.0)
        self.assertEqual(to_float("1"), 1.0)
        self.assertEqual(to_float("1e-3"), 0.001)
        self.assertEqual(to_float("adsf"), None)
        self.assertEqual(to_float("asdf", default_on_failure=False), "asdf")
        self.assertEqual(to_float("asdf", default=42), 42)
