# system modules
import unittest
import itertools

# internal modules
from polt.animator import Animator
from polt.animator.subplots import SubPlotsAnimator
from polt.animator.lines import LinesAnimator
from polt.animator.timelines import TimeLinesAnimator

# external modules
import matplotlib

matplotlib.use("agg")


class AnimatorTest(unittest.TestCase):
    def test_optimal_row_col_grid_gives_enough_plots(self):
        dim = (0.25, 0.5, 0.75, 1)
        for n, width, height in itertools.product(
            range(1, 10), dim, reversed(dim)
        ):
            rows, cols = SubPlotsAnimator.optimal_row_col_grid(
                object(), n=n, width=width, height=height
            )
            self.assertGreaterEqual(
                rows * cols,
                n,
                "For {n} plots, width {width} and height {height}, "
                "optimal_row_col_grid() returns a {rows}x{cols} grid, which "
                "only has {rowsxcols} cells, not room for {n}".format(
                    n=n,
                    height=height,
                    width=width,
                    rows=rows,
                    cols=cols,
                    rowsxcols=rows * cols,
                ),
            )

    def test_linesanimator_run(self):
        TimeLinesAnimator().run()
