# system modules
import unittest
import copy
from unittest.mock import patch
from functools import partial

# internal modules
from polt.filter import Filter
from polt.filter.metadata import MetaDataFilter
from polt.utils import *

# external modules


class TestFilter(Filter):
    def update_dataset(self, dataset):
        return dataset


class FilterChainTest(unittest.TestCase):
    def test_chain_empty(self):
        c = Filter.chain()
        for i in range(10):
            with self.subTest(input=i):
                self.assertEqual(c(i), i)

    def test_chain_squared_and_minus_one(self):
        c = Filter.chain(lambda x: x ** 2, lambda x: x - 1)
        for i in range(10):
            with self.subTest(input=i):
                self.assertEqual(c(i), i ** 2 - 1)


class FilterTest(unittest.TestCase):
    def setUp(self):
        self.f = TestFilter()

    def test_update_dataset_is_called_without_when_parser(self):
        d = {"parser": "asdf"}
        with patch.object(self.f, "update_dataset") as update_dataset_call:
            self.f(d)
            self.assertTrue(
                update_dataset_call.called,
                "update_dataset was not called although no when_parser "
                "restriction was set",
            )

    def test_update_dataset_not_called_when_parser_doesnt_match(self):
        self.f.when_parser = "fdsa"
        d = {"parser": "asdf"}
        with patch.object(self.f, "update_dataset") as update_dataset_call:
            self.f(d)
            self.assertFalse(
                update_dataset_call.called,
                "update_dataset was called although the when_parser condition "
                "was not met",
            )


class MetaDataFilterTest(unittest.TestCase):
    def setUp(self):
        self.f = MetaDataFilter()

    def test_no_change_without_set_properties(self):
        inputs = (
            {},
            {"parser": "asdf"},
            {"data": {}},
            {"data": {("bla", "bli", "blubb"): 1, "bla": [1, 2, 3]}},
        )
        for d in inputs:
            with self.subTest(dataset=d):
                self.assertDictEqual(
                    self.f(d.copy()),
                    d,
                    "MetaDataFilter without any "
                    "properties didn't leave dataset unchanged",
                )

    @staticmethod
    def metadata_equal(meta1, meta2):
        iter1 = iter(to_tuple(meta1))
        iter2 = iter(to_tuple(meta2))
        m1 = tuple(next(iter1, None) for i in range(10))
        m2 = tuple(next(iter2, None) for i in range(10))
        return m1 == m2

    @classmethod
    def matching_metadata(cls, data, meta):
        return filter(partial(cls.metadata_equal, meta), data)

    def assertMetadataChanged(
        self, orig_data, new_data, orig_meta, new_meta, *args, **kwargs
    ):
        self.assertFalse(
            any(self.metadata_equal(orig_meta, m) for m in new_data),
            "old metadata {} is still in the new dataset {}".format(
                repr(orig_meta), repr(new_data)
            ),
        )
        self.assertTrue(
            any(self.metadata_equal(new_meta, m) for m in new_data),
            "new metadata {} is not in the new dataset {}".format(
                repr(new_meta), repr(new_data)
            ),
        )
        orig_val = orig_data[
            next(self.matching_metadata(orig_data, orig_meta))
        ]
        new_val = new_data[next(self.matching_metadata(new_data, new_meta))]
        self.assertIs(
            orig_val,
            new_val,
            "original value {} of original matching metadata {} "
            "is not the same object as the new value {} "
            "of the new matching metadata {}".format(
                orig_val, orig_meta, new_val, new_meta
            ),
        )

    def test_renaming(self):
        dataset = {
            "data": {
                "quant1": 1,
                ("quant2", "unit2"): (1, 2, 3),
                ("quant3", None, "key3"): 3,
                (None, "unit2", "key4"): (4, 3, 2, 1),
            }
        }
        properties = {
            (("when_quantity", "quant1"), ("set_quantity", "internet")): {
                "quant1": ("internet",)
            },
            (("when_unit", "unit2"), ("set_key", "internet")): {
                ("quant2", "unit2"): ("quant2", "unit2", "internet"),
                (None, "unit2", "key4"): (None, "unit2", "internet"),
            },
            (("when_key", "key3"), ("set_quantity", "internet")): {
                ("quant3", None, "key3"): ("internet", None, "key3")
            },
            (("set_quantity", "internet"),): {
                "quant1": ("internet",),
                ("quant2", "unit2"): ("internet", "unit2"),
                ("quant3", None, "key3"): ("internet", None, "key3"),
                (None, "unit2", "key4"): ("internet", "unit2", "key4"),
            },
        }
        for props_seq, changes in properties.items():
            d = copy.deepcopy(dataset)
            props = dict(props_seq)
            m = MetaDataFilter()
            for prop, val in props.items():
                setattr(m, prop, val)
            updated = m(d)
            for old_meta, new_meta in changes.items():
                self.assertMetadataChanged(
                    dataset["data"], updated["data"], old_meta, new_meta
                )
