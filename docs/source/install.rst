
Installation
============

:mod:`polt` is best installed via :mod:`pip`::

    python3 -m pip install --user polt

This will install :mod:`polt` from `PyPi - the Python Package Index`_.

Depending on your setup it might be necessary to install the :mod:`pip` module
first:

.. code-block:: sh

    # Debian/Ubuntu
    sudo apt-get install python3-pip

Or see `Installing PIP`_.

Matplotlib
++++++++++

Furthermore, :mod:`matplotlib` (installed automatically alongside :mod:`polt`)
needs at least one working backend to be able to show the interactive plot
window. You probabily don't need to worry about this but if running the
:doc:`cli` fails with someting ugly sounding like ``No module named
'tkinter'``, then (at least on Debian/Ubuntu systems), running the following
will get you going:

.. code-block:: sh

    # Debian/Ubuntu
    sudo apt-get install python3-tk

.. _Installing PIP: https://pip.pypa.io/en/stable/installing/
.. _PyPI - the Python Package Index: https://pypi.org/project/polt/
