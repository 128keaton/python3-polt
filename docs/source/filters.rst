Filters
=======

Filters are used to modify gathered data before plotting it. See the :ref:`cli
live plotting data flow` for an overview of the data flow. By default, no
filtering is performed.

See :ref:`cli adding filters` to learn how to configure filters from the
:doc:`cli`.

.. _filters built-in:

Built-In Filters
++++++++++++++++

:mod:`polt` ships with a couple of built-in filters.

To get a list of available filters and their usable aliases, you may use the
:any:`EntryPointExtensions.get` function:

.. code:: python

   polt.extensions.EntryPointExtensions.get(polt.version.POLT_FILTER_ENTRY_POINT, aliases=True)
   # yields something like
   {('metadata',
     'MetaDataFilter',
     'polt.filter.metadata.MetaDataFilter'): polt.filter.metadata.MetaDataFilter}

You can also use :ref:`filters customization`.

:any:`Filter` is the base class for filters. It facilitates enabling filters
for specific parsers. For example, to apply a filter only to data from a parser
with :any:`Parser.name` ``"myparser"``, you can enable a :any:`Filter` only for
this parser by setting its :any:`Filter.when_parser` property (e.g.  via the
:doc:`cli`: ``polt add-filter -o when-parser=myparser``).

.. _filters metadatafilter:

Changing Metadata
-----------------

The :any:`MetaDataFilter` can be used to change metadata on-the-fly. This might
come in handy if you have no easy control over the metadata extracted by the
:doc:`parsers`. A :any:`MetaDataFilter` can have up to three conditions:
:any:`MetaDataFilter.when_quantity`, :any:`MetaDataFilter.when_unit` and
:any:`MetaDataFilter.when_key`. These can be set via the :doc:`cli` e.g. like
``polt add-filter -f metadata -o when-quantity=temperature``. If all of these
specified conditions match, the metadata is set according to
:any:`MetaDataFilter.set_quantity`, :any:`MetaDataFilter.set_unit` and
:any:`MetaDataFilter.set_key` if specified.

For example, if one of your :doc:`parsers` yields datasets without a unit
information:

.. code-block:: sh

   polt generate -r 3 -p 1 -c temperature='uniform(20,30)' -c pressure='uniform(900,1000)'
   # temperature,pressure
   # 21.9,928.7
   # 21.2,930.6
   # 23.1,918.4
   # 25.7,973.6
   # 22.1,951.0
   # 21.5,997.5
   # 23.2,950.5
   # 26.7,933.6
   # 24.8,990.2
   # ...

When you can't or don't want to modify the :any:`Parser` itself to add the unit
information, you can add the unit metadata by adding a :any:`MetaDataFilter`:

.. code-block:: sh

   polt generate -r 3 -p 1 -c temperature='uniform(20,30)' -c pressure='uniform(900,1000)' | \
   polt \
       add-source -p csv \
       add-filter -f metadata -o when-quantity=temperature -o set-unit='°C' \
       add-filter -f metadata -o when-quantity=pressure -o set-unit=hPa \
       live

.. image:: images/polt-metadatafilter.png

.. _filters customization:

Custom Filters
++++++++++++++

Similar to using :ref:`parsers customization` it is possible to implement your
own :any:`Filter`. For example if you are interested in tracking the maximum of
each quantity, you could implement a filter like this:


.. code-block:: python

    from collections import defaultdict
    from polt.filter import Filter
    from polt.utils import to_float, to_tuple


    class MaximumFilter(Filter):
        def __init__(self):
            # dictionary to record maxima of all quantities
            self.maxima = defaultdict(lambda: float("-inf"))

        def update_dataset(self, dataset):
            data = dataset.get("data", {})
            new_data = {}
            # iterate over all data slots that have something defined
            for key, val in filter(all, data.items()):
                # extract metadata from this data slot
                keyiter = iter(to_tuple(key))
                name, unit, key = (next(keyiter, None) for i in range(3))
                # determine old and new maximum
                old_max = self.maxima[name]
                new_max = max(
                    filter(lambda x: x is not None, map(to_float, to_tuple(val)))
                )
                # if this is a new record, remember and add it to the data
                if new_max > old_max:
                    self.maxima[name] = new_max
                    new_data[("maximum of {}".format(name), unit, key)] = new_max
            data.update(new_data)
            return dataset

Save this code to a file ``maximumfilter.py`` in the directory where you launch
the :doc:`cli` from. You can now use your custom filter like this:

.. code-block:: sh

    polt \
        add-source -o quantity=Quantity1 -c "polt generate -c walk -r 10 -s 42" \
        add-source -o quantity=Quantity2 -c "polt generate -c walk -r 10 -s 41" \
        add-filter -f maximumfilter.MaximumFilter \
        live -o extrapolate

.. image:: images/polt-custom-maximumfilter.png

See :ref:`extending polt` to learn how to make your custom filter globally
available.
