Parsers
=======

Parsers are used to extract data from sources. See the :ref:`cli live plotting
data flow` for an overwiew of how data is tranferred from the parser to the
plot.

.. _parsers built-in:

Built-In Parsers
++++++++++++++++

To get a list of available parsers and their usable aliases, you may use the
:any:`EntryPointExtensions.get` function:

.. code:: python

   polt.extensions.EntryPointExtensions.get(polt.version.POLT_PARSER_ENTRY_POINT, aliases=True)
   # yields something like
   {('csv',
     'CsvParser',
     'polt.parser.csvparser.CsvParser'): polt.parser.csvparser.CsvParser,
    ('numbers',
     'NumberParser',
     'polt.parser.numberparser.NumberParser'): polt.parser.numberparser.NumberParser}

You can also use :ref:`parsers customization`.

.. _parsers numberparser:

NumberParser
------------

The :any:`NumberParser` is a simple parser with the alias ``numbers`` reading
line-wise from the input and extracting simple floating-point numbers. It has
the options :any:`NumberParser.unit`, :any:`NumberParser.quantity` and
:any:`NumberParser.key` which you can set via the command-line:

.. code-block:: sh

   polt add-source -p numbers -o quantity=pressure -o unit=hPa -o key=sensor config
   # [source:stdin]
   # command = -
   # parser = polt.parser.numberparser.NumberParser
   # parser.quantity = pressure
   # parser.unit = hPa
   # parser.key = sensor


.. _parsers csvparser:

CsvParser
---------

The :any:`CsvParser` is a parser with alias ``csv`` based on the :mod:`csv`
module which (you guessed it) parses `CSV <csv on wikipedia_>`_. A `CSV <csv on
wikipedia_>`_ stream can contain many different quantitites.

See :ref:`cli adding sources specifying parser options` for how modify
:any:`CsvParser` options from the :doc:`cli`.

Extracting Metadata
'''''''''''''''''''

By default, the quantity name is the same as the column name.  However, the
option :any:`CsvParser.header_regex` can be used to extract the unit from the
column name. There exists a convenience way to use
:any:`CsvParser.HEADER_REGEX_UNIT_LAST` if your headers are formatted like
``quantity_unit``:

.. code-block:: sh

   polt generate \
        -c "temperature_celsius=uniform(20,25)" \
        -c "pressure_hPa=uniform(990,1020)" \
        --max-rate 5 \
        | polt \
            add-source -o name=Data -p csv -o header-regex=unit-last \
            live -o extrapolate=yes

.. image:: images/polt-csvparser-temperature-pressure.png

Equally, if your headers are formatted like ``key_quantity_unit``, you may use
:any:`CsvParser.HEADER_REGEX_KEY_QUANTITY_UNIT` via ``--option
header-regex=key-quantity-unit``. The key could be the sensor type for example.

Using a Custom Delimiter
''''''''''''''''''''''''

As `CSV <csv on wikipedia_>`_ is a very loose standard, the format may vary.
The column delimiter may be different from the default (``,``) but a semicolon
(``;``) for example. The :any:`CsvParser.delimiter` option can be used to
change this:

.. code-block:: sh

   polt generate \
        -c "temperature_celsius=uniform(20,25)" \
        -c "pressure_hPa=uniform(990,1020)" \
        --delimiter ";" \
        --max-rate 5 \
        | polt \
            add-source -o name=Data -p csv -o 'delimiter=;' \
            live -o extrapolate=yes

Selecting Columns
'''''''''''''''''

Selecting only specific columns can be achieved via
:any:`CsvParser.only_columns` (``--option only-columns=column1,column2,...``).
Blacklisting columns is implemented by :any:`CsvParser.not_columns` (``--option
not-columns=column3,column4,...``).

.. _parsers customization:

Custom Parsers
++++++++++++++

In case the built-in parsers :any:`NumberParser` and :any:`CsvParser` don't
do it for you, you can simply implement your own parser as subclass of
:any:`Parser`. For example if you had a source outputting the digits 0 to 9 as
literal words:

.. code-block:: sh

   while true;do
       for num in one three four nine eight seven;do
           echo $num;sleep 1
       done
   done
   # outputs forever:
   #
   # one
   # three
   # four
   # nine
   # eight
   # seven
   # ...

You would like to display the values with :mod:`polt`. Then write your custom
parser like this:

.. code:: python

    from polt.parser.parser import Parser


    class LiteralDigitParser(Parser):
        # create a mapping of words to numbers
        digits = {
            w: n
            for n, w in enumerate(
                (
                    "zero",
                    "one",
                    "two",
                    "three",
                    "four",
                    "five",
                    "six",
                    "seven",
                    "eight",
                    "nine",
                )
            )
        }

        @property
        def data(self): # the data generator property is all thats needed
            for line in self.f: # read a line from the input
                number = self.digits.get(line.strip(), None) # look for a match
                if number: # if a number was matched, yield the parsed dataset
                    yield {"number": number}


See :any:`Parser.data` for an explanation of what should be returned.  Save
this code to the file ``literaldigitparser.py`` in the directory where you
launch the :doc:`cli` from. You can then use your custom parser like this:

.. code-block:: sh

   while true;do
       for num in one three four nine eight seven;do
           echo $num;sleep 1
       done
   done | polt add-source --parser literaldigitparser.LiteralDigitParser live

Or as subprocess:

.. code-block:: sh

   polt \
       add-source \
           --cmd '/bin/sh -c '"'"'while true;do for num in one three four nine eight seven;do echo $num;sleep 1;done;done'"'"'' \
           --encoding utf8 \
           --name "Literal Digits" \
           --parser literaldigitparser.LiteralDigitParser \
    live

.. image:: images/polt-custom-literal-digits-parser.png
   :alt: Custom LiteralDigitsParser polt Output


.. note::

   The quote mess (``'"'"'``) in the ``--cmd`` is necessary because otherwise
   the ``$num`` variable would be replaced by our shell which is too early.

   The ``--encoding utf8`` option is necessary here because otherwise the
   command's stdout would be opened in plain bytes mode and our digit lookup
   (whose keys are strings, not bytes) would fail.

See :ref:`extending polt` to make your class globally available.

.. _csv on wikipedia: https://en.wikipedia.org/wiki/Comma-separated_values
