# Contributing

Development follows the [GitLab
Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/).

## Code Style

Code should be formatted with [`black`](https://github.com/ambv/black) and
follow [PEP8](https://www.python.org/dev/peps/pep-0008/).

Be sure to have run the following commands before pushing commits. Otherwise,
the CI pipeline will only pass with warnings.

```bash
pip3 install --user black pycodestyle autopep8
# optionally run autopep8
autopep8 -arij0 .
# format all Python files in black style
black -l79 .
# check PEP8 compatibility
pycodestyle .
```

## Versioning

This project uses [bumpversion](https://pypi.org/project/bumpversion/) to
increase version numbers.

## Tests

To run the tests, run

```bash
python3 setup.py test
```

To get a test coverage, run

```bash
# Install coverage
python3 -m pip install --user coverage
make coverage
```
